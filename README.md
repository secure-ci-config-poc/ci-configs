# Config Repo

## Motivation

The `.gitlab-ci.yml` configuration file should not be exposed to any user with the "developer" rule since it might grant unwanted access to variables and infrastructure or make different kinds of exploiting behaviour or simply unwanted changes possible.

## Solution

The currently best solutions seems to outsource the `.gitlab-ci.yml` file to another repository (see https://gitlab.com/gitlab-org/gitlab/-/issues/15632). However, a big drawback of this is that you can only have one configuration per repository.

This example repository tries to solve this and allow for branching of ci configs.

### Proof of Concept

This group has an example structure with two projects and one repository dedicated to holding ci/cd configurations of all projects. Access to the config repository can be granted selectively to Devops people, developers in the project repos don't have access to the ci/cd configuration.

Every project repository sets the config path in "Settings > CI/CD > General pipelines" to `ci-entrypoint.yml@secure-ci-config-poc/ci-configs`. The entrypoint config looks like this:

```
include:
  # By default load the configuration found in the folder
  # named after the project name that is running the pipeline.
  - local: '/$CI_PROJECT_NAME/.gitlab-ci.yml'
    rules:
      - if: '$CI_COMMIT_REF_NAME !~ /update-ci/'
  
  # If the ref/branch name in the project running the pipeline contains
  # the string 'update-ci', we use the same branch in this config repo
  # to load the ci config.
  - project: 'secure-ci-config-poc/ci-configs'
    ref: $CI_COMMIT_REF_NAME
    file: '/$CI_PROJECT_NAME/.gitlab-ci.yml'
    rules:
      - if: '$CI_COMMIT_REF_NAME =~ /update-ci/'
```

Essentially, each project has its own folder named after the project name, containing a `.gitlab-ci.yml` and if a project branch contains "updated-ci" it uses the config from the same branch name from the config repository.

**Example:**

1. We created a new branch "chore/project-a-update-ci-custom-test" (containing our keyword "update-ci") in this config repository and changed something for the pipeline in "Project A". See [this MR](https://gitlab.com/secure-ci-config-poc/ci-configs/-/merge_requests/1)
2. In order to test the changes before merging, we [created a branch](https://gitlab.com/secure-ci-config-poc/project-a/-/commits/chore/project-a-update-ci-custom-test) with the exact same name ("chore/project-a-update-ci-custom-test") in project a. Note that the pipeline runs "test-updated" and not "test-default" as on the main branch.
3. Any commit we push in our new branch in Project A will use the changed ci config.

**Downsides:**

- Fairly complex compared to normal workflow and using hardcoded keywords
- Need to create branches in both repos
- The branch in Project A does have no meaning except for testing pipelines
- You have to push something in Project A each time you change the config here.
  - Reloading pipelines does not seem to use updated configuration

